import {
  CONTACTS_FETCH_REQUEST,
  CONTACTS_FETCH_SUCCESS,
  CONTACTS_FETCH_FAILED,
  CONTACT_CREATE_REQUEST,
  CONTACT_CREATE_SUCCESS,
  CONTACT_CREATE_FAILED,
  CONTACT_DELETE_REQUEST,
  CONTACT_EDIT_REQUEST,
  CONTACT_FILTER_REQUEST
} from '../constants/contacts'

export const contactsGetRequest = payload => ({ type: CONTACTS_FETCH_REQUEST, payload })
export const contactsGetSuccess = payload => ({ type: CONTACTS_FETCH_SUCCESS, payload })
export const contactsGetFailed = payload => ({ type: CONTACTS_FETCH_FAILED, payload })

export const contactCreateRequest = payload => ({ type: CONTACT_CREATE_REQUEST, payload })
export const contactCreateSuccess = payload => ({ type: CONTACT_CREATE_SUCCESS, payload })
export const contactCreateFailed = payload => ({ type: CONTACT_CREATE_FAILED, payload })

export const contactDeleteRequest = payload => ({ type: CONTACT_DELETE_REQUEST, payload })

export const contactEditRequest = payload => ({ type: CONTACT_EDIT_REQUEST, payload })

export const contactsFilter = payload => ({ type: CONTACT_FILTER_REQUEST, payload })
