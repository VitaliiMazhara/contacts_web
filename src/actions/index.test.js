import * as actions from '../actions/index'
import * as types from '../constants/contacts'

describe('actions', () => {
  it('should create an action to add a query fetch data', () => {
    const payload = 'Text'
    const expectedAction = {
      type: types.CONTACTS_FETCH_REQUEST,
      payload
    }
    expect(actions.contactsGetRequest(payload)).toEqual(expectedAction)
  })
})
