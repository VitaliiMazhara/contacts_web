import {createTextMask} from 'redux-form-input-masks'

export const phoneMask = createTextMask({
  pattern: '(999) 999-9999'
})