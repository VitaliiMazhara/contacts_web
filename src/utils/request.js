function parseJSON(response) {
  return response.json()
}

export default function request(url, options) {
  return fetch(url, options)
    .then(parseJSON)
    .catch(function(e) {
      if (e.name === 'TypeError') {
        console.log('Network error! ', e)
      } else {
        throw e
      }
    })
}
