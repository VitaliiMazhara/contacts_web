import axios from 'axios'
import { API_URL } from '../constants/api'

export const api = axios.create({
  baseURL: API_URL
})

export const fetchContacts = url => {
  return api({
    method: 'GET',
    url
  }).then(res => res)
}

export const createContact = ({ firstName, lastName, phonesCombine }, callback) => {
  return api({
    method: 'POST',
    url: 'contacts',
    data: {
      first_name: firstName,
      last_name: lastName,
      phone_numbers: phonesCombine
    }
  })
    .then(res => res)
    .then(() => {
      callback()
    })
}

export const editContact = ({ id, firstName, lastName, phonesCombine }) => {
  return api({
    method: 'PUT',
    url: `contacts/${id}`,
    data: {
      first_name: firstName,
      last_name: lastName,
      phone_numbers: phonesCombine
    }
  })
    .then(res => res)
}

export const deleteContact = id => {
  return api({
    method: 'DELETE',
    url: `contacts/${id}`
  }).then(res => res)
}
