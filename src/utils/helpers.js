export const truncateString = (string, countString) => {
  if (string.length > countString) {
    return string.substring(0, countString) + '...'
  } else {
    return string
  }
}
