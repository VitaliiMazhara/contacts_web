import React from 'react'
import { Field } from 'redux-form'
import Button from '../components/Button'
import { trashIcon } from '../constants/svg'
import { FormRowST } from '../styles/forms'
import { phoneMask } from './masks';

export const renderInputField = ({
  input,
  label,
  type,
  placeholder,
  meta: { asyncValidating, touched, error }
}) => (
  <div>
    <label>{label}</label>
    <div className={asyncValidating ? 'async-validating' : ''}>
      <input {...input} type={type} placeholder={placeholder} />
      {touched && error && <span>{error}</span>}
    </div>
  </div>
)

export const renderPhones = ({ fields, meta: { error, submitFailed } }) => (
  <div>
    {fields.map((phone, index) => (
      <FormRowST key={index}>
        <Field
          name={`${phone}.phone`}
          label={`Phone number ${index + 2}`}
          type="text"
          component={renderInputField}
          {...phoneMask}
        />
        <Button className="form-remove-phone" handleButton={() => fields.remove(index)}>
          {trashIcon}
        </Button>
      </FormRowST>
    ))}
    <div>
      {fields.length < 2 ? (
        <Button  className="form-add-phone" typeButton="button" onClick={() => fields.push({})}>
          Add phone #{fields.length + 2}
        </Button>
      ) : null}
      {submitFailed && error && <span>{error}</span>}
    </div>
  </div>
)
