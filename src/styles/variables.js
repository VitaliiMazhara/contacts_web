// Colors
export const MAIN_BG_COLOR = '#E28B65'

export const MAIN_TEXT_COLOR = '#544b4b'
export const SECOND_TEXT_COLOR = '#fff'

export const NOT_ACTIVE_COLOR = '#d2d2d2'


// Effects
export const TRANSITION = '.3s'
export const TRANSITION_MEDIUM = '.5s'
export const TRANSITION_SLOW = '1.5s'
// Header

// Search

// Contacts
