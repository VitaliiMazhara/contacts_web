import styled from 'styled-components'
import { ButtonST } from '../components/Button/styles'
import { MAIN_TEXT_COLOR } from './variables'

export const FormST = styled.form`
  padding: 0;
`

export const FormRowST = styled.div`
  position: relative;
  margin-top: 15px;
  color: ${MAIN_TEXT_COLOR};
  label {
    margin-bottom: 5px;
  }
  input {
    width: 320px;
    height: 40px;
    padding: 0 10px;
  }
  ${ButtonST} {
    height: 40px;
    width: 50%;
    font-family: MontserratRegular;
    margin: 0 auto;
    font-size: 18px;
  }

  .form-remove-phone {
    position: absolute;
    width: 40px;
    bottom: 0;
    right: 0;
    button {
      color: red;
      font-size: 14px;
      svg {
        fill: red;
      }
    }
  }

  .form-add-phone {
    button {
      color: ${MAIN_TEXT_COLOR};
    }
  }
`
