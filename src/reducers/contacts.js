import {
  CONTACTS_FETCH_REQUEST,
  CONTACTS_FETCH_SUCCESS,
  CONTACTS_FETCH_FAILED,
  CONTACT_CREATE_REQUEST,
  CONTACT_CREATE_FAILED,
  CONTACT_CREATE_SUCCESS,
  CONTACT_DELETE_FAILED,
  CONTACT_DELETE_SUCCESS,
  CONTACT_DELETE_REQUEST,
  CONTACT_EDIT_FAILED,
  CONTACT_EDIT_SUCCESS,
  CONTACT_EDIT_REQUEST,
  CONTACT_FILTER_REQUEST
} from '../constants/contacts'

const initialState = {
  contactList: [],
  isLoading: false,
  isLoadingCreateUser: false,
  isLoadingEditUser: false,
  isLoadingDeleteUser: false,
  filteredContacts: null,
  error: null
}

export default function contacts(state = initialState, action) {
  switch (action.type) {
    case CONTACTS_FETCH_REQUEST:
      return {
        ...state,
        isLoading: true
      }
    case CONTACT_CREATE_REQUEST:
      return {
        ...state,
        isLoadingCreateUser: true
      }
    case CONTACT_EDIT_REQUEST:
      return {
        ...state,
        isLoadingEditUser: true
      }
    case CONTACT_DELETE_REQUEST:
      return {
        ...state,
        isLoadingDeleteUser: true
      }

    case CONTACTS_FETCH_SUCCESS:
      return {
        ...state,
        contactList: action.payload,
        isLoading: false
      }
    case CONTACT_CREATE_SUCCESS:
      return {
        ...state,
        isLoadingCreateUser: false
      }
    case CONTACT_EDIT_SUCCESS:
      return {
        ...state,
        isLoadingEditUser: false
      }
    case CONTACT_DELETE_SUCCESS:
      return {
        ...state,
        isLoadingDeleteUser: false
      }

    case CONTACTS_FETCH_FAILED:
      return {
        ...state,
        error: action.error,
        isLoading: false
      }
    case CONTACT_CREATE_FAILED:
      return {
        ...state,
        isLoadingCreateUser: false
      }
    case CONTACT_EDIT_FAILED:
      return {
        ...state,
        isLoadingEditUser: false
      }
    case CONTACT_DELETE_FAILED:
      return {
        ...state,
        isLoadingDeleteUser: false
      }

    case CONTACT_FILTER_REQUEST:
      let contactsAll = state.contactList
      let searchQuery = action.payload.toLowerCase()

      let filteredContacts = contactsAll.filter(item => {
        let lastName = item.last_name.toLowerCase()
        return lastName.indexOf(searchQuery) !== -1
      })

      return {
        ...state,
        filteredContacts
      }
    default:
      return state
  }
}
