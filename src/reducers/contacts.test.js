import reducer from './contacts'
import * as actions from '../actions/index'
import * as types from '../constants/contacts'

describe('contacts reducer', () => {
  const initialState = {
    contactList: [],
    isLoading: false,
    isLoadingCreateUser: false,
    isLoadingEditUser: false,
    isLoadingDeleteUser: false,
    filteredContacts: null,
    error: null
  }

  it('should return the initial state', () => {
    expect(reducer(undefined, {})).toEqual(initialState)
  })

  it('should handle CONTACTS_FETCH_REQUEST', () => {
    expect(reducer({}, { type: types.CONTACTS_FETCH_REQUEST, isLoading: true })).toEqual({
      isLoading: true
    })
  })

  it('should handle CONTACTS_FETCH_SUCCESS', () => {
    const expectedPayload = {
      isLoading: false,
      contactList: [{}, {}]
    }
    expect(
      reducer(initialState, {
        type: types.CONTACTS_FETCH_SUCCESS,
        payload: [{}, {}]
      })
    ).toEqual({ ...initialState, ...expectedPayload })
  })

  it('should handle CONTACTS_FETCH_FAILED', () => {
    const expectedPayload = {
      isLoading: false,
      error: 'Error'
    }
    expect(
      reducer(initialState, {
        type: types.CONTACTS_FETCH_FAILED,
        error: 'Error'
      })
    ).toEqual({ ...initialState, ...expectedPayload })
  })
})
