import contactsSaga from './contacts'

function* rootSaga() {
  yield contactsSaga
}

export default rootSaga
