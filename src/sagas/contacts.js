// @flow
import { call, put, takeLatest, fork } from 'redux-saga/effects'
import {
  CONTACTS_FETCH_REQUEST,
  CONTACTS_FETCH_FAILED,
  CONTACTS_FETCH_SUCCESS,
  CONTACT_CREATE_SUCCESS,
  CONTACT_CREATE_FAILED,
  CONTACT_CREATE_REQUEST,
  CONTACT_DELETE_REQUEST,
  CONTACT_DELETE_SUCCESS,
  CONTACT_DELETE_FAILED,
  CONTACT_EDIT_SUCCESS,
  CONTACT_EDIT_FAILED,
  CONTACT_EDIT_REQUEST
} from '../constants/contacts'
import { fetchContacts, createContact, deleteContact, editContact } from '../utils/api'
import { contactsGetRequest } from '../actions';

/**
 * Workers
 */

export function* workerContacts(action) {
  try {
    const contacts = yield call(fetchContacts, 'contacts')
    yield put({ type: CONTACTS_FETCH_SUCCESS, payload: contacts.data })
  } catch (e) {
    yield put({ type: CONTACTS_FETCH_FAILED, error: e.message })
  }
}

function* workerCreateContact(action): Generator<*, *, *> {
  try {
    const contact = yield call(createContact, action.payload)
    yield put({ type: CONTACT_CREATE_SUCCESS, payload: contact })

  } catch (e) {
    yield put({ type: CONTACT_CREATE_FAILED, error: e.message })
  }
  yield put(contactsGetRequest())
}

function* workerEditContact(action): Generator<*, *, *> {
  try {
    const contact = yield call(editContact, action.payload)
    yield put({ type: CONTACT_EDIT_SUCCESS, payload: contact })
    yield put(contactsGetRequest())
  } catch (e) {
    yield put({ type: CONTACT_EDIT_FAILED, error: e.message })
  }
}

function* workerDeleteContact(action): Generator<*, *, *> {
  try {
    const contact = yield call(deleteContact, action.payload)
    yield put({ type: CONTACT_DELETE_SUCCESS, payload: contact })
    yield put(contactsGetRequest())
  } catch (e) {
    yield put({ type: CONTACT_DELETE_FAILED, error: e.message })
  }
}

/**
 * Watchers
 */

function* watchContacts(): Generator<*, *, *> {
  yield takeLatest(CONTACTS_FETCH_REQUEST, workerContacts)
}

function* watchContactCreate(): Generator<*, *, *> {
  yield takeLatest(CONTACT_CREATE_REQUEST, workerCreateContact)
}

function* watchEditCreate(): Generator<*, *, *> {
  yield takeLatest(CONTACT_EDIT_REQUEST, workerEditContact)
}

function* watchDeleteCreate(): Generator<*, *, *> {
  yield takeLatest(CONTACT_DELETE_REQUEST, workerDeleteContact)
}

export default [
  fork(watchContacts),
  fork(watchContactCreate),
  fork(watchDeleteCreate),
  fork(watchEditCreate)
]
