// @flow
import React, { Component } from 'react'
import { connect } from 'react-redux'

import Header from '../../components/Header'
import ContactsList from '../../components/ContactsList'
import Search from '../Search'
import { AppST } from '../../styles/app'

import {
  contactsGetRequest,
  contactCreateRequest,
  contactDeleteRequest,
  contactEditRequest
} from '../../actions'

type Props = {
  contactCreateRequest: any,
  dispatch: any,
  contacts: any
}

class App extends Component<Props> {
  componentDidMount() {
    this.requestContacts()
  }

  requestContacts = () => {
    const { dispatch } = this.props
    dispatch(contactsGetRequest())
  }

  createContact = ({ firstName, lastName, phone, phones }) => {
    const { dispatch } = this.props
    let phonesCombine: Array<string> = []
    phonesCombine.push(phone)
    if (phones && phones.length) {
      phones.forEach(item => {
        phonesCombine.push(item.phone)
      })
    }

    dispatch(contactCreateRequest({ firstName, lastName, phonesCombine }))
  }

  editContact = id => ({ firstName, lastName, phone, phones }) => {
    const { dispatch } = this.props
    let phonesCombine: Array<string> = []
    phonesCombine.push(phone)
    if (phones && phones.length) {
      phones.forEach(item => {
        phonesCombine.push(item.phone)
      })
    }
    dispatch(contactEditRequest({ id, firstName, lastName, phonesCombine }))
  }

  deleteContact = id => e => {
    const { dispatch } = this.props
    dispatch(contactDeleteRequest(id))
  }

  render() {
    const {
      contacts: { contactList, filteredContacts }
    } = this.props

    return (
      <AppST>
        <Header  createContact={this.createContact} />
        <Search />
        <ContactsList
          contactList={filteredContacts || contactList}
          deleteContact={this.deleteContact}
          editContact={this.editContact}
        />
      </AppST>
    )
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts
  }
}

export default connect(mapStateToProps)(App)
