// @flow
import React from 'react'
import { Row, Col } from 'reactstrap'
import { connect } from 'react-redux'

import { SearchMainST } from './style.js'
import Input from '../../components/Input/index.js'
import { contactsFilter } from '../../actions/index.js'

type Props = {
  dispatch: any,
  contactsFilterDispatcher: any
}

class Search extends React.Component<Props> {
  render() {
    const { contactsFilterDispatcher } = this.props
    return (
      <SearchMainST>
        <Row>
          <Col xs="12">
            <Input
              id="search-input"
              type="text"
              placeholder="Search by surname"
              onKeyUpSearchInput={contactsFilterDispatcher}
            />
          </Col>
        </Row>
      </SearchMainST>
    )
  }
}

const mapStateToProps = state => {
  return {
    contacts: state.contacts
  }
}

const mapDispatchToProps = (dispatch, e) => {
  return {
    contactsFilterDispatcher: e => {
      dispatch(contactsFilter(e.target.value))
    }
  }
}

export default connect(
  mapStateToProps,
  mapDispatchToProps
)(Search)
