import styled from 'styled-components'
import { MAIN_BG_COLOR, SECOND_TEXT_COLOR } from '../../styles/variables'

export const HeaderST = styled.div`
  display: flex;
  justify-content: space-between;
  height: 100px;
  background-color: ${MAIN_BG_COLOR};
  color: ${SECOND_TEXT_COLOR};

  .header {
    &-wrapper {
      height: 100%;
    }
    &-title {
      display: flex;
      justify-content: flex-start;
      align-items: center;
      font-size: 22px;
    }
    &-button {
      display: flex;
      justify-content: flex-end;
      align-items: center;
    }
  }
`
