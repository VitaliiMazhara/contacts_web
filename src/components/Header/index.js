// @flow

import React from 'react'
import { Container, Row, Col } from 'reactstrap'

import { HeaderST } from './style'
import Button from '../Button'
import { addIcon } from '../../constants/svg'
import Modal from '../Modal'
import CreateContactForm from '../CreateContactForm'

type Props = {
  createContact: any,
  closeModal: any
}

type State = {
  showModal: boolean
}

class Header extends React.Component<Props, State> {
  constructor() {
    super()

    this.state = {
      showModal: false
    }
  }

  handleShowModal = () => {
    this.setState({ showModal: true })
  }

  handleHideModal = () => {
    this.setState({ showModal: false })
  }
  

  render() {
    const { createContact } = this.props

    const modal = this.state.showModal ? (
      <Modal handleButton={this.handleHideModal}>
        <CreateContactForm closeModal={this.handleHideModal} createContact={createContact} />
      </Modal>
    ) : null
    return (
      <HeaderST>
        <Container>
          <Row className="header-wrapper">
            <Col xs="6" className="header-title">
              All Contacts
            </Col>
            <Col xs="6" className="header-button">
              <Button handleButton={this.handleShowModal}>{addIcon}</Button>
            </Col>
            {modal}
          </Row>
        </Container>
      </HeaderST>
    )
  }
}

export default Header
