import React from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'
import { renderInputField, renderPhones } from '../../utils/forms'
import Button from '../Button'
import { FormST, FormRowST } from '../../styles/forms'
import { phoneMask } from '../../utils/masks'

class DeleteContactForm extends React.Component {
  componentDidMount() {
    this.props.initialize({
      firstName: this.props.item.first_name,
      lastName: this.props.item.last_name,
      phone: this.props.item.phone_numbers[0],
      phones: ['dffdfdf']
    })
  }

  handleForm = id => values => {
    this.props.editContact(id)(values)
    this.props.closeModal()
  }

  render() {
    const { handleSubmit, editContact, item } = this.props
    return (
      <FormST onSubmit={handleSubmit(this.handleForm(item.id))}>
        <FormRowST>
          <Field name="firstName" label="First name" component={renderInputField} type="text" />
        </FormRowST>
        <FormRowST>
          <Field name="lastName" label="Last name" component={renderInputField} type="text" />
        </FormRowST>
        <FormRowST>
          <Field
            name="phone"
            label="Phone number"
            component={renderInputField}
            type="text"
            {...phoneMask}
          />
        </FormRowST>
        <FormRowST>
          <FieldArray name="phones" component={renderPhones} />
        </FormRowST>
        <FormRowST>
          <Button successStyle typeButton="submit">
            Submit
          </Button>
        </FormRowST>
      </FormST>
    )
  }
}

DeleteContactForm = reduxForm({
  form: 'create_delete_form'
})(DeleteContactForm)

export default DeleteContactForm
