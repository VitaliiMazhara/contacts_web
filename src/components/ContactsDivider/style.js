import styled from 'styled-components'

export const ContactsDividerST = styled.div`
  background-color: #fafafa;
  padding: 20px;
  font-family: MontserratRegular
`
