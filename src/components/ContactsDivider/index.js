import React from 'react'
import { ContactsDividerST } from './style'

class ContactsDivider extends React.Component {
  render() {
    const { item } = this.props
    return <ContactsDividerST>{item}</ContactsDividerST>
  }
}

export default ContactsDivider
