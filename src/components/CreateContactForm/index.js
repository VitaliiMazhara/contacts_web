import React from 'react'
import { Field, FieldArray, reduxForm } from 'redux-form'
import { renderInputField, renderPhones } from '../../utils/forms'
import Button from '../Button'
import { FormST, FormRowST } from '../../styles/forms'
import { phoneMask } from '../../utils/masks'

class CreateContactForm extends React.Component {
  combineMethods = values => {
    this.props.createContact(values)
    this.props.closeModal()
  }
  render() {
    const { handleSubmit } = this.props
    return (
      <FormST onSubmit={handleSubmit(this.combineMethods)}>
        <FormRowST>
          <Field name="firstName" label="First name" component={renderInputField} type="text" />
        </FormRowST>
        <FormRowST>
          <Field name="lastName" label="Last name" component={renderInputField} type="text" />
        </FormRowST>
        <FormRowST>
          <Field
            name="phone"
            label="Phone number"
            component={renderInputField}
            type="text"
            {...phoneMask}
          />
        </FormRowST>
        <FormRowST>
          <FieldArray name="phones" component={renderPhones} />
        </FormRowST>
        <FormRowST>
          <Button successStyle typeButton="submit">
            Submit
          </Button>
        </FormRowST>
      </FormST>
    )
  }
}

CreateContactForm = reduxForm({
  form: 'create_contact_form'
})(CreateContactForm)

export default CreateContactForm
