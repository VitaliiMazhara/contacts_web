import styled from 'styled-components'
import { TRANSITION, MAIN_TEXT_COLOR, SECOND_TEXT_COLOR } from '../../styles/variables'

export const ButtonST = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 30px;
  width: 30px;
  border: none;
  background-color: transparent;
  cursor: pointer;
  transition: ${TRANSITION};
  width: ${props => (props.successStyle ? 'auto' : null)};
  padding: ${props => (props.successStyle ? 0 : '5px')};
  button {
    width: 100%;
    height: 100%;
    padding: 0;
    border: none;
    cursor: pointer;
    outline: none;
    color: #fff;
    font-weight: 400;
    background-color: ${props => (props.successStyle ? '#26C55C' : 'transparent')};

    &:hover {
      background-color: ${props => (props.successStyle ? '#39ad619e' : 'transparent')};
    }
  }
  &:focus {
    outline: none;
  }
  &:hover {
    svg {
      fill: ${MAIN_TEXT_COLOR};
    }
  }
  svg {
    width: 100%;
    height: 100%;
    transition: ${TRANSITION};
    fill: ${SECOND_TEXT_COLOR};
  }
`
