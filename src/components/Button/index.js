// @flow

import React from 'react'
import { ButtonST } from './styles'

type Props = {
  children: string,
  link?: boolean,
  to?: string,
  typeButton?: string,
  handleButton: (id: number) => (event: any) => void,
  successStyle?: string
}

class Button extends React.Component<Props> {
  render() {
    const { link, to, handleButton, typeButton } = this.props
    if (link) {
      return (
        <ButtonST>
          <a href={to}>{this.props.children}</a>
        </ButtonST>
      )
    } else {
      return (
        <ButtonST {...this.props}>
          <button onClick={handleButton} type={typeButton}>
            {this.props.children}
          </button>
        </ButtonST>
      )
    }
  }
}

export default Button
