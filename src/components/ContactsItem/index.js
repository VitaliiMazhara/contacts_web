// @flow
import React from 'react'
import { ContactsItemST } from './style'
import Button from '../Button'
import { truncateString } from '../../utils/helpers'
import { editIcon, callIcon, trashIcon } from '../../constants/svg'
import Modal from '../Modal'
import DeleteContactForm from '../DeleteContactForm'

type Props = {
  item: any
}

type State = {
  showModal: boolean
}

class ContactsItem extends React.Component<Props, State> {
  constructor() {
    super()

    this.state = {
      showModal: false
    }
  }

  handleShowModal = () => {
    this.setState({ showModal: true })
  }

  handleHideModal = () => {
    this.setState({ showModal: false })
  }
  render() {
    const {
      item: { first_name, last_name, phone_numbers, id },
      deleteContact,
      editContact,
    } = this.props

    const fullNameString: string = `${last_name} ${first_name}`
    const phoneString: string = phone_numbers[0]

    const modal = this.state.showModal ? (
      <Modal handleButton={this.handleHideModal}>
        <DeleteContactForm
          item={this.props.item}
          editContact={editContact}
          closeModal={this.handleHideModal}
        />
      </Modal>
    ) : null

    return (
      <ContactsItemST>
        <div className="info-wrapper">
          <div className="info-name">{truncateString(fullNameString, 35)}</div>
          <div>{truncateString(phoneString, 20)}</div>
        </div>
        <div className="buttons-wrapper">
          <Button link to={`tel:${phoneString}`}>
            {callIcon}
          </Button>
          <Button handleButton={this.handleShowModal}>{editIcon}</Button>
          <Button handleButton={deleteContact(id)}>{trashIcon}</Button>
        </div>
        {modal}
      </ContactsItemST>
    )
  }
}

export default ContactsItem
