import styled from 'styled-components'
import {  MAIN_TEXT_COLOR } from '../../styles/variables'
import { ButtonST } from '../Button/styles'

export const ContactsItemST = styled.div`
  display: flex;
  justify-content: space-between;
  align-items: center;
  height: 50px;
  padding: 0 20px;
  align-items: center;
  font-size: 18px;
  color: ${MAIN_TEXT_COLOR}

  .buttons-wrapper{
    display: flex;
  }

  ${ButtonST}{
    &:first-child{
      svg{
        fill: green;
      }
    }

    &:last-child{
      svg{
        fill: red;
      }
    }

    &:last-child{
      svg{
        fill: red;
      }
    }
    &:hover{
      opacity: .6;
    }
    svg{
      fill: ${MAIN_TEXT_COLOR};
    }
  }

  
  .info{
    &-wrapper{
      display: flex;
      width: 80%;
    }
    &-name{
      width: 330px;
    }
  }
`
