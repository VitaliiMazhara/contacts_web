// @flow
import React from 'react'
import { InputST } from './style'

type Props = {
  type: string,
  id: string,
  placeholder: string,
  onKeyUpSearchInput: any
}

class Input extends React.Component<Props> {
  render() {
    const { type, id, placeholder, onKeyUpSearchInput } = this.props
    return (
      <InputST>
        <div className="input-wrapper">
          <input id={id} type={type} placeholder={placeholder} onKeyUp={onKeyUpSearchInput} />
          <div className="input-pseudo" />
        </div>
      </InputST>
    )
  }
}

export default Input
