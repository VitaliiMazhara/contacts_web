import styled from 'styled-components'
import {
  MAIN_TEXT_COLOR,
  TRANSITION_MEDIUM,
  NOT_ACTIVE_COLOR
} from '../../styles/variables'

export const InputST = styled.div`
  display: flex;
  justify-content: center;
  align-items: center;
  height: 100px;
  .input-wrapper {
    position: relative;
    width: 70%;
    height: 50px;
  }
  .input-pseudo {
    position: absolute;
    overflow: hidden;
    width: 100%;
    height: 2px;
    &:after {
      content: '';
      position: absolute;
      width: 100%;
      height: 2px;
      bottom: 0;
      left: 0;
      right: 0;
      background-color: ${NOT_ACTIVE_COLOR};
      transition: ${TRANSITION_MEDIUM};
    }
    &:before {
      content: '';
      position: absolute;
      width: 100%;
      height: 2px;
      bottom: 0;
      left: -100%;
      background-color: ${MAIN_TEXT_COLOR};
      transition: ${TRANSITION_MEDIUM};
    }
  }
  input {
    width: 100%;
    height: 100%;
    padding: 0 20px;
    border: none;
    outline: none;
    font-size: 22px;
    color: ${MAIN_TEXT_COLOR};
    transition: 2s;
    &:focus {
      & ~ .input-pseudo {
        &:after {
          left: 100%;
          transition: left ${TRANSITION_MEDIUM} ease-in-out;
        }
        &:before {
          left: 0;
          transition: left ${TRANSITION_MEDIUM} ease-in-out;
        }
      }
    }
  }
`
