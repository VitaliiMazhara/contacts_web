import styled from 'styled-components'
import { Container } from 'reactstrap'
import { NOT_ACTIVE_COLOR } from '../../styles/variables';

export const ContactsPageST = styled(Container)`
  .list-wrapper {
    width: 70%;
    margin: 0 auto 100px;
    .contacts-item_parent {
      &:not(:last-child) {
        border-bottom: 1px solid ${NOT_ACTIVE_COLOR};
      }
    }
  }
`
