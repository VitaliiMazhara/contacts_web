// @flow
import React from 'react'
import { Col, Row } from 'reactstrap'

import { ContactsPageST } from './style'
import ContactsItem from '../ContactsItem'
import ContactsDivider from '../ContactsDivider'

type Props = {
  contactList: Array<any>,
  deleteContact: any,
  editContact: any
}

class ContactsList extends React.Component<Props> {
  render() {
    const { contactList, deleteContact, editContact } = this.props

    return (
      <ContactsPageST>
        <Row>
          <Col>
            <div className="list-wrapper">
              {contactList.map((it, i, arr) => (
                <div className="contacts-item_parent" key={i}>
                  {i > 0 &&
                  it.last_name.toUpperCase().charAt(0) !==
                    arr[i - 1].last_name.toUpperCase().charAt(0) ? (
                    <ContactsDivider item={it.last_name.toUpperCase().charAt(0)} />
                  ) : i === 0 ? (
                    <ContactsDivider item={it.last_name.toUpperCase().charAt(0)} />
                  ) : null}

                  <ContactsItem
                    key={i}
                    item={it}
                    deleteContact={deleteContact}
                    editContact={editContact}
                  />
                </div>
              ))}
            </div>
          </Col>
        </Row>
      </ContactsPageST>
    )
  }
}

export default ContactsList
