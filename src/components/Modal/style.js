import styled from 'styled-components'

export const ModalST = styled.div`
  display: flex;
  align-items: center;
  justify-content: center;
  font-family: 'MontserratLight';
  position: fixed;
  width: 100%;
  height: 100%;
  top: 0;
  left: 0;
  cursor: pointer;
  .modal-background {
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background-color: #fff;
    opacity: 0.9;
  }
  .modal-header {
    position: fixed;
    top: 0;
    left:0;
    width: 100%;
    height: 100px;
    padding: 0;
    &_row {
      height: 100%;
    }
    &_container {
      height: 100%;
    }
  }
  .modal-button {
    display: flex;
    justify-content: flex-end;
    align-items: center;
    svg {
      fill: red;
    }
  }
  .modal-body {
    display: flex;
    justify-content: center;
    align-items: center;
    height: auto;
    width: auto;
    position: absolute;
  }
`
