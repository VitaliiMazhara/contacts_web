import React from 'react'
import Enzyme, { mount } from 'enzyme'
import Adapter from 'enzyme-adapter-react-16'
import Modal from './index'

Enzyme.configure({ adapter: new Adapter() })
function setup() {
  const props = {
    handleButton: jest.fn()
  }
  const enzymeWrapper = mount(<Modal {...props} />)
  return {
    props,
    enzymeWrapper
  }
}

describe('components', () => {
  describe('Modal', () => {
    it('should render self and subcomponents', () => {
      const { enzymeWrapper, props } = setup()
      
      const button = enzymeWrapper.find('Button').props()
      expect(button.handleButton).toBe(props.handleButton)
    })
  })
})
