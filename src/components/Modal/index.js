import React from 'react'
import ReactDOM from 'react-dom'
import { Container, Row, Col } from 'reactstrap'

import { ModalST } from './style'
import Button from '../Button'
import { closeIcon } from '../../constants/svg'

class Modal extends React.Component {
  constructor() {
    super()

    this.root = document.createElement('div')
  }
  componentDidMount() {
    document.body.appendChild(this.root)
  }

  componentWillUnmount() {
    document.body.removeChild(this.root)
  }

  render() {
    const { handleButton } = this.props
    return ReactDOM.createPortal(
      <ModalST>
        <div className="modal-background" onClick={handleButton} />
        <div className="modal-body">
          <div>{this.props.children}</div>
        </div>
        <div className="modal-header">
          <Container className="modal-header_container">
            <Row className="modal-header_row">
              <Col xs={{ size: '6', offset: 6 }} className="modal-button">
                <Button handleButton={handleButton}>{closeIcon}</Button>
              </Col>
            </Row>
          </Container>
        </div>
      </ModalST>,
      this.root
    )
  }
}

export default Modal
